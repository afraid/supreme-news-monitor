
## Supreme News Monitor

A barebone monitor built by [@notchefbob](https://twitter.com/notchefbob).

---

## Installation

This monitor requires [Node.js](https://nodejs.org/) v8+ to run.

1. Install the dependencies ```npm install ```
2. Modify `config.json` with your webhook link (Discord & Slack supported). **If you are using Discord, put ```/slack``` at the end of the webhook url**.
3. Run ```npm start``` to start the application.

---

## License
[MIT](https://mit-license.org/)

---