const request = require("request-promise");
const cheerio = require("cheerio");
const moment = require("moment")

//Load user configuration
const USER_CONFIG = require("./config.json");

//URL we will be scraping from
const NEWS_URL = "https://www.supremenewyork.com/news/archive";

//This will hold the latest ID scraped from the news page
var LATEST_ID = null;

//Basic sleep function which uses a promise
const sleep = ms => new Promise(res => setTimeout(res, ms));

//This is the function the monitor will run from
function monitorNews() {
    var newsReq = {
        url: NEWS_URL,
        method: "GET",
    };

    request(newsReq)
        .then(parseArchive)
        .catch(handleError)
}

async function parseArchive(returnedHTML) {
    var $ = cheerio.load(returnedHTML);

    //This variable contains the latest href to an article
    var latestNewsHref = $($('a[href*="/news"]')[0]).attr("href");
    //This contains the latest news integer based id
    var latestInt = parseInt(latestNewsHref.split("/news/")[1]);
    if (LATEST_ID == null) {
        //For first runs, this will automatically set the latestID
        LATEST_ID = latestInt
        monitorLog(`FIRST RUN - CURRENT NEWS ID ${LATEST_ID}`)
        //Sleep for 5 seconds
        await sleep(5000);
        //Retry function
        monitorNews();
    } else if (latestInt > LATEST_ID) {
        //If we detect a new news article

        monitorLog(`NEW NEWS ARTICLE - ${latestInt}`)
        //Call our next function to pull information from the latest article & send to our webhook
        await gatherNewsInfo(latestInt)

        //SET THE NEW NEWS ID
        LATEST_ID = latestInt

        //Sleep for 5 seconds
        await sleep(5000);
        //Retry function
        monitorNews();
    } else {
        //If there is no news detected
        monitorLog(`NO NEW NEWS`)
        //Sleep for 5 seconds
        await sleep(5000);
        //Retry function
        monitorNews();
    }
}

//Simple error handler
async function handleError(returnedError) {
    //Log the error we get
    console.error(returnedError.message);
    //Sleep for 5 seconds
    await sleep(5000);
    //Retry function
    monitorNews();
}

function gatherNewsInfo(latestNewsID) {
    var articleReq = {
        url: `https://www.supremenewyork.com/news/${latestNewsID}`,
        method: "GET",
    };

    request(articleReq)
        .then(parseNews)
        .catch(handleError)
}

async function parseNews(returnedHTML) {
    var $ = cheerio.load(returnedHTML);

    //Collect information for the webhook
    var mainImageURL = `https:${$('#main-img').attr("src")}`
    var articleTitle = $('#container > div > article > h2').text();
    var articleDate = $("#container > div > article > time").text();
    var articleInformation = $("#container > div > article > div.blurb").text().replace(/\s\s+/g, '\n\n');


    //This variable contains our webhook payload
    var webhookJSON = {
        attachments: [{
            color: "#ff0000",
            title: `${articleDate} | ${articleTitle}`,
            title_link: `https://www.supremenewyork.com/news/${LATEST_ID}`,
            fields: [{
                title: "News Information",
                value: articleInformation
            }],
            image_url: mainImageURL
        }]
    }

    await postWebhook(webhookJSON)
}

function postWebhook(webhookJSON) {
    //Contains our request options
    var webhookReq = {
        url: USER_CONFIG.webhookURL,
        method: "POST",
        json: webhookJSON
    }
    request(webhookReq)
        .then(() => {
            monitorLog("NEWS UPDATE SENT SUCCESSFULLY")
        })
}

//Basic message logger
function monitorLog(message) {
    console.log(`[${moment(Date.now()).format("HH:mm:ss.SSS")}] [SUPREME NEWS MONITOR] [${message}]`);
}

//Function to begin the process
function main() {
    if (!USER_CONFIG.webhookURL) {
        monitorLog("NO WEBHOOK URL DETECTED - EXITING");
        process.exit(1)
    }
    monitorLog("STARTING");
    monitorNews();
}

main();